from services.pars_users_data import UserService


TEST_FILE_PATH = 'tests/faked/intelli_test_task.csv'


def tests_user_service_pars_data():
    user_service = UserService(TEST_FILE_PATH)
    user_service.read_csv_file()
    user_service.pars_data()
    valid_rows = 6
    assert valid_rows == len(user_service.df.index)
