import argparse
import os

from services import pars_users_data
from config.db import engine


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Pars users data')
    parser.add_argument('file_path', type=str, nargs='?', help='the path to the file to be parsed', default='intelli_test_task.csv')
    args = parser.parse_args()
    if not os.path.isfile(args.file_path):
        raise ValueError('file not found!')
    user_service = pars_users_data.UserService(file_path=args.file_path)
    user_service.read_csv_file()
    user_service.pars_data()
    user_service.save_to_db(engine)
    print(user_service.df)

