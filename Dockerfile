FROM python:3.9-slim-buster

ENV ROOT_DIR /usr/src/app

RUN apt-get update && \
    apt-get install -y build-essential && \
    apt-get install -y libpq-dev && \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false && \
    rm -rf /var/lib/apt/lists/*

COPY . $ROOT_DIR

COPY ./requirements.txt /requirements.txt
RUN pip install --upgrade pip && \
    pip install --no-cache-dir -r /requirements.txt && \
    rm -f /requirements.txt

WORKDIR $ROOT_DIR

ENTRYPOINT ["python", "./main.py"]
