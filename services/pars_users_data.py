import pandas as pd
import numpy as np


from typing import Optional
from sqlalchemy import String, INT, BOOLEAN, BIGINT
from sqlalchemy.engine import Engine


class UserService:
    file_path: Optional[str] = None
    df: Optional[pd.DataFrame] = None

    def __init__(self, file_path: Optional[str] = None, df: Optional[pd.DataFrame] = None) -> None:
        self.file_path = file_path
        self.df = df

    def read_csv_file(self) -> pd.DataFrame:
        assert self.file_path

        if not self.file_path.endswith('.csv'):
            raise ValueError('File of wrong format!')

        self.df = pd.read_csv(self.file_path, delimiter=',')

        return self.df

    def pars_data(self) -> pd.DataFrame:
        self.df.replace(r'^\s*$', np.nan, regex=True, inplace=True)
        self.df.dropna(subset=['full_name', 'language', 'email'], inplace=True)
        self.df.sort_values(by=['id'], inplace=True)
        self.df[['first_name', 'last_name']] = self.df.full_name.str.split(' ', expand=True)
        self.df['last_login_at_ts'] = self._date_time_to_ts('last_login_at')
        self.df['date_of_birth_ts'] = self._date_time_to_ts('date_of_birth')
        self.df['created_at_ts'] = self._date_time_to_ts('created_at')
        self.df['gender_counter'] = self._counter_rows('gender')
        self.df['language_counter'] = self._counter_rows('language')
        self.df['profile_occupancy'] = self.df['profile_percentage'] >= 90
        self.df.drop(['full_name', 'last_login_at', 'date_of_birth', 'created_at'], axis=1, inplace=True)
        return self.df

    def save_to_db(self, engine: Engine, table_name: str = 'users') -> None:
        dtype = {
            'id': BIGINT,
            'first_name': String(128),
            'last_name': String(128),
            'language': String(8),
            'language_counter': INT,
            'gender': String(16),
            'gender_counter': INT,
            'email': String(256),
            'profile_percentage': INT,
            'profile_occupancy': BOOLEAN,
            'last_login_at_ts': INT,
            'date_of_birth_ts': INT,
            'created_at_ts': INT,
        }
        self.df.to_sql(con=engine, name=table_name, dtype=dtype, if_exists='append', index=False)

    def _counter_rows(self, column_name: str):
        return self.df.sort_values([column_name], ascending=True).groupby([column_name]).cumcount() + 1

    def _date_time_to_ts(self, column_name: str):
        return pd.to_datetime(self.df[column_name]).astype(np.int64) // 10 ** 9
